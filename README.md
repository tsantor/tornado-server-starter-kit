# Tornado Starter Kit
Author: Tim Santor <tsantor@xstudios.com>

## Installation
As a developer I assume you have `pyenv` and `make` installed.

```bash
make env
make reqs
```

## Config
Create a file `~/project/config/server.cfg`:

```conf
[default]
host=0.0.0.0
port=8080
working_dir = ~/project/
static_dir = ~/project/static
```

## Server

```bash
make serve
```
