# -*- coding: utf-8 -*-

import argparse
import configparser
import logging
import os
import signal

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler

# https://www.tornadoweb.org/en/stable/web.html
# https://infinitescript.com/2017/06/making-requests-non-blocking-in-tornado/

logger = logging.getLogger(__name__)

# Shut up these packages
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("asyncio").setLevel(logging.WARNING)

# -----------------------------------------------------------------------------


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config", help="Config file path", default="~/project/config/server.cfg"
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose output")
    return parser.parse_args()


def get_config(args):
    """Read config"""
    config_file = os.path.expanduser(args.config)
    config = configparser.ConfigParser()
    config.read(config_file)
    return config


def _handle_shutdown_signals(signum, frame):
    assert signum in (signal.SIGINT, signal.SIGTERM)
    logger.info("signal -%d received", signum)
    logger.info("Received graceful shutdown request")
    # IOLoop.current().add_callback(_shutdown)
    IOLoop.current().add_callback_from_signal(_shutdown)


def _shutdown():
    IOLoop.current().stop()


class My404Handler(RequestHandler):
    # Override prepare() instead of get() to cover all possible HTTP methods.
    def prepare(self):
        self.set_status(404)
        # self.render('404.html')
        self.write("This is not the droid you are looking for.")
        self.finish()


class MyRequestHandler(RequestHandler):
    def write_error(self, status_code, **kwargs):
        """Write a consistent json error response."""
        # print(kwargs)
        self.set_status(status_code)
        response = {"error": {"code": status_code, "message": str(kwargs["error"])}}
        self.write(response)
        self.finish()

    def write_response(self, data):
        """Write a consistent json response."""
        response = {"data": data}
        self.write(response)
        self.finish()


class DefaultHandler(RequestHandler):
    """Default handler"""

    def get_template_namespace(self):
        namespace = super().get_template_namespace()
        namespace.update({"name": "Tim"})
        return namespace

    def get(self):
        # self.write("These are not the droids you are looking for...")
        # self.finish()

        # template = pkg_resources.resource_filename(__name__, 'templates/client-test.html')
        template = "index.html"
        self.render(template)


class HelloWorldHandler(MyRequestHandler):
    def get(self, *args, **kwargs):
        response = "Hello world!"
        self.write_response(response)

    # def post(self, *args, **kwargs):
    #     # Get the payload posted
    #     payload = self.request.body
    #     data = json.loads(payload)
    #     logger.debug(data)

    #     # Do stuff your processing here

    #     response = {"baz": "bat", "image": "static/9789526532998.jpg"}
    #     self.write_response(response)


def get_tornado_app(config):
    """Return a tornado web application instance which includes url config"""

    # Set app settings
    settings = dict(
        # cookie_secret=config.get('default', 'cookie_secret'),
        # xsrf_cookies=True,
        # template_path=os.path.join(os.path.dirname(__file__), 'templates'),
        # static_path=os.path.join(os.path.dirname(__file__), 'static'),
        # static_path=os.path.expanduser(config.get("default", "static_dir")),
        # static_url_prefix="/static/",
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        default_handler_class=My404Handler,
        debug=True,
        # static_hash_cache=False,
    )

    urls = [
        (r"/", DefaultHandler),
        # (r'/static/(.*)', StaticFileHandler, {'path': os.path.expanduser(path)}),
    ]

    return Application(urls, **settings)


def main():
    """Run script."""
    args = get_args()
    config = get_config(args)

    # Setup logging
    logging.basicConfig(
        level=logging.INFO,
        format="[%(levelname)s] [%(asctime)s] [%(name)s] %(message)s",
        datefmt="%Y-%m-%d %I:%M:%S %p",
    )

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
        # logger.setLevel(logging.DEBUG)

    # work_dir = os.path.expanduser(config.get("default", "working_dir"))

    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    signal.signal(signal.SIGINT, _handle_shutdown_signals)
    signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Get config items
    host = config.get("default", "host")
    port = config.getint("default", "port")

    logger.debug("Listening on %s:%d...", host, port)

    # Create Tornado application
    app = get_tornado_app(config)
    httpserver = HTTPServer(app)
    httpserver.listen(port, address=host)

    IOLoop.current().start()

    httpserver.stop()
    logger.info("Server shut down gracefully")


if __name__ == "__main__":
    main()
