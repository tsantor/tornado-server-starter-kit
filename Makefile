env:
	pyenv virtualenv 3.9.4 tornado_env && pyenv local tornado_env

reqs:
	python -m pip install -U pip black pylint
	# pre-commit && pre-commit install
	python -m pip install -r requirements.txt

install:
	python -m pip install -e .

serve:
	# tornado-server -v
	cd myproject && python tornado_server.py -v
